using System;

namespace Tests.Entities
{
	class CustomTypesEntity
	{
		public int id;
		public string name;

		/// <summary>
		/// In the db, there will be an integer that represents the day of the year starting from january, 1st.
		/// </summary>
		public DateTime date_this_year;
	}
}