using System;

namespace Tests.Entities
{
	public class StoreItem
	{
		public int storeid;
		public int itemid;
		public string name;
		public string[] tags;
		public int[] sizes_available;
		public int amount_available;
	}
}
