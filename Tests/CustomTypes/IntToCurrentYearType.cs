using System;
using SqlBuilder.Types;

namespace Tests
{
	class IntToCurrentYearType : IUserType
	{
		public object ReadValueFromDb (object val)
		{
			int x = (int)val;
			return new DateTime(DateTime.Now.Year, 1, 1).AddDays(x - 1);
		}

		public object SetValueToAssign (object val)
		{
			return ((DateTime)val).DayOfYear;
		}
	}
}
