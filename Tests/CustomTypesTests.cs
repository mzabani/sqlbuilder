using System;
using SqlBuilder;
using SqlBuilder.Types;
using System.Linq;
using System.Collections.Generic;
using NUnit.Framework;
using Tests.Mocking;
using Tests.Entities;

namespace Tests
{
	[TestFixture()]
	public class CustomTypes
	{
		[Test()]
		public void TestArrayPropertyValues() {
			// Register our custom type
			RegisteredCustomTypes.RegisterCustomType<DateTime, int>(new IntToCurrentYearType());

			var readerBuilder = new IntToCurrentYearCommand();
			
			// Adds the query results
			readerBuilder.AddQueryResult(new object[] {
				1,
				"any name will do",
				32 // February first
			});

			var resultsFetcher = new ResultsFetcher<CustomTypesEntity>(readerBuilder.CustomTypesEntityProjectionMap);
			IList<CustomTypesEntity> queryResults = resultsFetcher.List(readerBuilder.GetCommand());
			
			var firstRow = queryResults.First();
			Assert.AreEqual(new DateTime(DateTime.Now.Year, 2, 1), firstRow.date_this_year);
		}
	}
}
