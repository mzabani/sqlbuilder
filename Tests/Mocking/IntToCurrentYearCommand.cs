using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Moq;
using Tests.Entities;

namespace Tests.Mocking
{
	class IntToCurrentYearCommand
	{
		private Stack<object[]> QueryResults;
		private object[] CurrentResult;
		private Mock<IDataReader> ReaderMock;

		public IDictionary<string, string> CustomTypesEntityProjectionMap {
			get
			{
				return new Dictionary<string, string>() {
					{ "id", "id" },
					{ "name", "name" },
					{ "day_of_the_year", "date_this_year" }
				};
			}
		}

		/// <summary>
		/// Returns the queried projections in the order they would come from the database.
		/// </summary>
		public IEnumerable<string> GetOrderedColumnsNames() {
			yield return "id";
			yield return "name";
			yield return "day_of_the_year";
		}

		private object ArrayStyleAccess(int ordinal) {
			return CurrentResult[ordinal];
		}

		private int SimulateGetOrdinal(string projectionName) {
			return GetOrderedColumnsNames().TakeWhile(c => c != projectionName).Count();
		}

		/// <summary>
		/// (Re)mocks array-style access and IDataReader methods to simulate a different result being returned by the database.
		/// </summary>
		private bool SimulateRead() {
			if (QueryResults.Count == 0)
				return false;

			CurrentResult = QueryResults.Pop();
			return true;
		}

		public IDbCommand GetCommand() {
			var commandMock = new Mock<IDbCommand>();

			commandMock.Setup(c => c.ExecuteReader()).Returns(ReaderMock.Object);

			return commandMock.Object;
		}

		/// <summary>
		/// No null values are allowed yet.
		/// </summary>
		/// <param name="columns">Columns.</param>
		public void AddQueryResult(object[] columns) {
			if (columns == null)
				throw new ArgumentNullException("columns");
			if (columns.Any (x => x == null))
				throw new ArgumentException("All columns must be non null");

			QueryResults.Push(columns);
		}

		public IntToCurrentYearCommand()
		{
			QueryResults = new Stack<object[]>();
			ReaderMock = new Mock<IDataReader>();

			// Read() must be mocked!
			ReaderMock.Setup(m => m.Read())
					  .Returns(SimulateRead);

			// Array style access too!
			ReaderMock.Setup(m => m.GetValue(It.IsAny<int>())).Returns((int ordinal) => ArrayStyleAccess(ordinal));
			ReaderMock.Setup(m => m.GetOrdinal(It.IsAny<string>())).Returns((string colname) => SimulateGetOrdinal(colname));

			// And of course IsDBNull.. for now nothing is null..
			ReaderMock.Setup(m => m.IsDBNull(It.IsAny<int>())).Returns(false);
		}
	}
}
